/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.4.27-MariaDB : Database - cafeteria_proyectoabc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cafeteria_proyectoabc` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `cafeteria_proyectoabc`;

/*Table structure for table `cliente` */

DROP TABLE IF EXISTS `cliente`;

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `carrera` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `cliente` */

insert  into `cliente`(`id_cliente`,`nombre`,`carrera`,`status`) values (1,'Roberto Carlos','Informatica',1),(2,'Ricardo López','Logistica y Transporte',1),(3,'Alejandra Guzmán','Administracion de Empresas',1),(4,'Sofia Núñez','Informatica',1),(5,'Luis Fernando','Biotecnologia',1);

/*Table structure for table `compra` */

DROP TABLE IF EXISTS `compra`;

CREATE TABLE `compra` (
  `id_compra` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  `fk_proveedor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_compra`),
  KEY `fk_proveedor` (`fk_proveedor`),
  CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`fk_proveedor`) REFERENCES `proveedor` (`id_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `compra` */

insert  into `compra`(`id_compra`,`fecha`,`total`,`fk_proveedor`) values (1,'2023-06-15',200,1),(2,'2023-06-16',150,2);

/*Table structure for table `descuento` */

DROP TABLE IF EXISTS `descuento`;

CREATE TABLE `descuento` (
  `id_descuento` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(30) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `descuento_fijo` tinyint(1) DEFAULT NULL,
  `descuento_porcentaje` tinyint(1) DEFAULT NULL,
  `cantidad_fijo` float DEFAULT NULL,
  `cantidad_porcentaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_descuento`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `descuento` */

insert  into `descuento`(`id_descuento`,`codigo`,`fechaInicio`,`fechaFin`,`descuento_fijo`,`descuento_porcentaje`,`cantidad_fijo`,`cantidad_porcentaje`) values (1,'DESC10','2023-06-01','2023-06-30',0,1,NULL,10),(2,'DESC20','2023-06-15','2023-07-15',0,1,NULL,20),(3,'DESC5','2023-06-20','2023-07-20',0,1,NULL,5),(4,'FIJO50','2023-07-01','2023-07-31',1,0,50,NULL),(5,'FIJO100','2023-07-15','2023-08-15',1,0,100,NULL);

/*Table structure for table `detallecompra` */

DROP TABLE IF EXISTS `detallecompra`;

CREATE TABLE `detallecompra` (
  `id_detalleCompra` int(11) NOT NULL AUTO_INCREMENT,
  `fk_compra` int(11) DEFAULT NULL,
  `fk_ingrediente` int(11) DEFAULT NULL,
  `fk_producto` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unidad` float DEFAULT NULL,
  PRIMARY KEY (`id_detalleCompra`),
  KEY `fk_compra` (`fk_compra`),
  KEY `fk_ingrediente` (`fk_ingrediente`),
  KEY `detallecompra_ibfk_3` (`fk_producto`),
  CONSTRAINT `detallecompra_ibfk_1` FOREIGN KEY (`fk_compra`) REFERENCES `compra` (`id_compra`),
  CONSTRAINT `detallecompra_ibfk_2` FOREIGN KEY (`fk_ingrediente`) REFERENCES `ingrediente` (`id_ingrediente`),
  CONSTRAINT `detallecompra_ibfk_3` FOREIGN KEY (`fk_producto`) REFERENCES `producto` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detallecompra` */

insert  into `detallecompra`(`id_detalleCompra`,`fk_compra`,`fk_ingrediente`,`fk_producto`,`cantidad`,`precio_unidad`) values (1,1,1,NULL,50,1.5),(2,2,2,NULL,100,2);

/*Table structure for table `detalleplatillo` */

DROP TABLE IF EXISTS `detalleplatillo`;

CREATE TABLE `detalleplatillo` (
  `fk_platillo` int(11) NOT NULL,
  `fk_ingrediente` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`fk_platillo`,`fk_ingrediente`),
  KEY `fk_ingrediente` (`fk_ingrediente`),
  CONSTRAINT `detalleplatillo_ibfk_1` FOREIGN KEY (`fk_platillo`) REFERENCES `platillo` (`id_platillo`),
  CONSTRAINT `detalleplatillo_ibfk_2` FOREIGN KEY (`fk_ingrediente`) REFERENCES `ingrediente` (`id_ingrediente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detalleplatillo` */

insert  into `detalleplatillo`(`fk_platillo`,`fk_ingrediente`,`cantidad`) values (1,1,1),(1,2,2),(2,3,1),(2,4,1),(3,5,1);

/*Table structure for table `detallesorden` */

DROP TABLE IF EXISTS `detallesorden`;

CREATE TABLE `detallesorden` (
  `folio` int(11) NOT NULL AUTO_INCREMENT,
  `fk_venta` int(11) DEFAULT NULL,
  `fk_producto` int(11) DEFAULT NULL,
  `fk_descuento` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unidad` float DEFAULT NULL,
  PRIMARY KEY (`folio`),
  KEY `fk_descuento` (`fk_descuento`),
  KEY `fk_venta` (`fk_venta`),
  KEY `fk_producto` (`fk_producto`),
  CONSTRAINT `detallesorden_ibfk_1` FOREIGN KEY (`fk_descuento`) REFERENCES `descuento` (`id_descuento`),
  CONSTRAINT `detallesorden_ibfk_2` FOREIGN KEY (`fk_venta`) REFERENCES `ventas` (`folio`),
  CONSTRAINT `detallesorden_ibfk_3` FOREIGN KEY (`fk_producto`) REFERENCES `producto` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detallesorden` */

insert  into `detallesorden`(`folio`,`fk_venta`,`fk_producto`,`fk_descuento`,`cantidad`,`precio_unidad`) values (1,1,1,1,2,15.5),(2,1,1,1,5,15.5),(3,2,2,2,10,12),(4,3,3,3,7,18),(5,4,4,4,12,14.5),(6,5,5,5,15,20),(7,1,1,1,200,15.5),(8,1,1,1,2,15.5),(9,1,1,1,2,15.5),(10,1,1,1,2,15.5),(11,6,2,1,3,12),(12,7,1,1,3,12),(13,1,1,1,5,15.5),(14,7,2,1,5,60);

/*Table structure for table `detalleventaplatillo` */

DROP TABLE IF EXISTS `detalleventaplatillo`;

CREATE TABLE `detalleventaplatillo` (
  `id_detalleVentaPlatillo` int(11) NOT NULL AUTO_INCREMENT,
  `fk_venta` int(11) DEFAULT NULL,
  `fk_detalleOrden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_detalleVentaPlatillo`),
  KEY `fk_venta` (`fk_venta`),
  KEY `fk_detalleOrden` (`fk_detalleOrden`),
  CONSTRAINT `detalleventaplatillo_ibfk_1` FOREIGN KEY (`fk_venta`) REFERENCES `ventas` (`folio`),
  CONSTRAINT `detalleventaplatillo_ibfk_2` FOREIGN KEY (`fk_detalleOrden`) REFERENCES `detallesorden` (`folio`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detalleventaplatillo` */

insert  into `detalleventaplatillo`(`id_detalleVentaPlatillo`,`fk_venta`,`fk_detalleOrden`) values (1,1,1),(2,1,1),(3,2,2),(4,3,3),(5,4,4),(6,5,5),(7,7,11),(8,1,12),(9,1,12);

/*Table structure for table `empleado` */

DROP TABLE IF EXISTS `empleado`;

CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `cargo` varchar(30) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `contrasena_hash` varchar(255) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `empleado` */

insert  into `empleado`(`id_empleado`,`nombre`,`telefono`,`cargo`,`correo`,`contrasena_hash`,`status`) values (1,'Juan Pérez','5551234567','Cajero','correo1@correo.com','5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',1),(2,'María García','5557654321','Gerente','correo2@correo.com','5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',1),(3,'Pedro Rodríguez','5559876543','Cocinero','correo3@correo.com','5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',1),(4,'Laura López','5554567890','Mesero','correo4@correo.com','5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',1),(5,'Carlos Martínez','5556543210','Mesero','correo5@correo.com','5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',1),(6,'María López','5559876543','Cajero','correo6@correo.com','d1cbe4ce6c598e5e195db5833d1fec3294b9a0bc410d4ea5a0816157e9ddb0cb',1);

/*Table structure for table `formadepago` */

DROP TABLE IF EXISTS `formadepago`;

CREATE TABLE `formadepago` (
  `id_formaDePago` int(11) NOT NULL AUTO_INCREMENT,
  `tarjeta` tinyint(1) DEFAULT NULL,
  `efectivo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_formaDePago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `formadepago` */

insert  into `formadepago`(`id_formaDePago`,`tarjeta`,`efectivo`) values (1,0,1),(2,1,0),(3,1,1);

/*Table structure for table `gasto` */

DROP TABLE IF EXISTS `gasto`;

CREATE TABLE `gasto` (
  `id_gasto` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `fk_tipoGasto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_gasto`),
  KEY `fk_tipoGasto` (`fk_tipoGasto`),
  CONSTRAINT `gasto_ibfk_1` FOREIGN KEY (`fk_tipoGasto`) REFERENCES `tipogasto` (`id_tipoGasto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `gasto` */

insert  into `gasto`(`id_gasto`,`fecha`,`cantidad`,`fk_tipoGasto`) values (1,'2023-06-30',5000,1),(2,'2023-06-29',1000,2),(3,'2023-06-28',500,3);

/*Table structure for table `impuestos` */

DROP TABLE IF EXISTS `impuestos`;

CREATE TABLE `impuestos` (
  `folio` int(11) NOT NULL AUTO_INCREMENT,
  `fk_venta` int(11) DEFAULT NULL,
  `fk_compra` int(11) DEFAULT NULL,
  `iva_total` float DEFAULT NULL,
  PRIMARY KEY (`folio`),
  KEY `fk_venta` (`fk_venta`),
  KEY `fk_compra` (`fk_compra`),
  CONSTRAINT `impuestos_ibfk_1` FOREIGN KEY (`fk_venta`) REFERENCES `ventas` (`folio`),
  CONSTRAINT `impuestos_ibfk_2` FOREIGN KEY (`fk_compra`) REFERENCES `compra` (`id_compra`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `impuestos` */

insert  into `impuestos`(`folio`,`fk_venta`,`fk_compra`,`iva_total`) values (5,1,NULL,16),(6,2,NULL,32),(7,NULL,1,32),(8,5,NULL,48);

/*Table structure for table `ingrediente` */

DROP TABLE IF EXISTS `ingrediente`;

CREATE TABLE `ingrediente` (
  `id_ingrediente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `costo` float DEFAULT NULL,
  PRIMARY KEY (`id_ingrediente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `ingrediente` */

insert  into `ingrediente`(`id_ingrediente`,`nombre`,`costo`) values (1,'Pan de torta',3),(2,'Jamón',1.5),(3,'Atún',2),(4,'Verduras para ensalada',1),(5,'Hot Dog Bread',2.5),(6,'Salchicha para hotdog',4);

/*Table structure for table `inventario` */

DROP TABLE IF EXISTS `inventario`;

CREATE TABLE `inventario` (
  `id_inventario` int(11) NOT NULL AUTO_INCREMENT,
  `fk_ingrediente` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_inventario`),
  KEY `fk_ingrediente` (`fk_ingrediente`),
  CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`fk_ingrediente`) REFERENCES `ingrediente` (`id_ingrediente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `inventario` */

insert  into `inventario`(`id_inventario`,`fk_ingrediente`,`cantidad`) values (1,1,300),(2,2,300),(3,3,300),(4,4,300),(5,5,250),(6,6,200);

/*Table structure for table `platillo` */

DROP TABLE IF EXISTS `platillo`;

CREATE TABLE `platillo` (
  `id_platillo` int(11) NOT NULL AUTO_INCREMENT,
  `fk_producto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_platillo`),
  KEY `fk_producto` (`fk_producto`),
  CONSTRAINT `platillo_ibfk_1` FOREIGN KEY (`fk_producto`) REFERENCES `producto` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `platillo` */

insert  into `platillo`(`id_platillo`,`fk_producto`) values (1,1),(2,3),(3,5);

/*Table structure for table `producto` */

DROP TABLE IF EXISTS `producto`;

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `importe` float DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `es_platillo` tinyint(1) DEFAULT NULL,
  `cantidad_en_stock` int(11) DEFAULT 0,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `producto` */

insert  into `producto`(`id_producto`,`importe`,`descripcion`,`es_platillo`,`cantidad_en_stock`,`status`) values (1,15.5,'Torta de jamón',1,100,1),(2,12,'Cafe americano',0,95,1),(3,18,'Ensalada de atun',1,100,1),(4,14.5,'Refresco lata',0,100,1),(5,20,'Hot Dog',1,100,1);

/*Table structure for table `proveedor` */

DROP TABLE IF EXISTS `proveedor`;

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `representante` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `proveedor` */

insert  into `proveedor`(`id_proveedor`,`nombre`,`telefono`,`correo`,`direccion`,`representante`,`status`) values (1,'Proveedor 1','555-123-4567','proveedor1@correo.com','Boulevard Centenario 1297','Jaime',1),(2,'Proveedor 2','555-123-4568','proveedor2@correo.com','Camaron Sabalo 2121 Norte','Antonio',1);

/*Table structure for table `tipogasto` */

DROP TABLE IF EXISTS `tipogasto`;

CREATE TABLE `tipogasto` (
  `id_tipoGasto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` longtext DEFAULT NULL,
  PRIMARY KEY (`id_tipoGasto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `tipogasto` */

insert  into `tipogasto`(`id_tipoGasto`,`descripcion`) values (1,'Salarios'),(2,'Mantenimiento'),(3,'Servicios básicos');

/*Table structure for table `ventas` */

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas` (
  `folio` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `RFC` varchar(20) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `fk_empleado` int(11) DEFAULT NULL,
  `fk_cliente` int(11) DEFAULT NULL,
  `fk_formasDePago` int(11) DEFAULT NULL,
  PRIMARY KEY (`folio`),
  KEY `fk_empleado` (`fk_empleado`),
  KEY `fk_cliente` (`fk_cliente`),
  KEY `fk_formasDePago` (`fk_formasDePago`),
  CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`fk_empleado`) REFERENCES `empleado` (`id_empleado`),
  CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`fk_cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`fk_formasDePago`) REFERENCES `formadepago` (`id_formaDePago`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `ventas` */

insert  into `ventas`(`folio`,`fecha`,`lugar`,`RFC`,`total`,`fk_empleado`,`fk_cliente`,`fk_formasDePago`) values (1,'2023-06-01','Calle 123','RFC123456789',100,1,1,1),(2,'2023-06-02','Avenida 456','RFC234567890',200,2,2,2),(3,'2023-06-03','Boulevard 789','RFC345678901',150,3,3,3),(4,'2023-06-04','Callejón 012','RFC456789012',250,4,4,1),(5,'2023-06-05','Camino 345','RFC567890123',300,5,5,2),(6,'2023-07-01','Avenida de prueba','RFC678901234',200,1,1,1),(7,'2023-07-01','Avenida de prueba','RFC678901234',200,1,1,1);

/* Trigger structure for table `compra` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `calcular_iva_compras` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `calcular_iva_compras` AFTER INSERT ON `compra` FOR EACH ROW BEGIN
  DECLARE iva FLOAT;
  SET iva = NEW.total * 0.16;
  INSERT INTO impuestos (fk_compra, iva_total) VALUES (NEW.id_compra, iva);
END */$$


DELIMITER ;

/* Trigger structure for table `detallesorden` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `reducir_stock_productos` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `reducir_stock_productos` AFTER INSERT ON `detallesorden` FOR EACH ROW BEGIN
    DECLARE esPlatillo TINYINT;
    DECLARE cantidadEnInventario INT;
    DECLARE cantidadEnStock INT;
    -- Checamos si el producto vendido es un platillo
    SELECT es_platillo INTO esPlatillo FROM producto WHERE id_producto = NEW.fk_producto;
    IF esPlatillo = 0 THEN
        -- Si el producto no es un platillo, reducimos la cantidad del producto en stock
        SELECT cantidad_en_stock INTO cantidadEnStock FROM producto WHERE id_producto = NEW.fk_producto;
        IF cantidadEnStock >= NEW.cantidad THEN
            UPDATE producto
            SET cantidad_en_stock = cantidad_en_stock - NEW.cantidad
            WHERE id_producto = NEW.fk_producto;
        ELSE
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'No hay suficientes productos en stock para realizar la venta';
        END IF;
    END IF;
END */$$


DELIMITER ;

/* Trigger structure for table `detalleventaplatillo` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `reducir_stock_ingredientes` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `reducir_stock_ingredientes` AFTER INSERT ON `detalleventaplatillo` FOR EACH ROW BEGIN
    -- Aquí, reducimos la cantidad de cada ingrediente usado en el platillo vendido
    UPDATE inventario, detalleplatillo, platillo, detallesorden
    SET inventario.cantidad = inventario.cantidad - (detalleplatillo.cantidad * detallesorden.cantidad)
    WHERE detallesorden.folio = NEW.fk_detalleOrden 
    AND detallesorden.fk_producto = platillo.fk_producto 
    AND platillo.id_platillo = detalleplatillo.fk_platillo 
    AND inventario.fk_ingrediente = detalleplatillo.fk_ingrediente;
END */$$


DELIMITER ;

/* Trigger structure for table `inventario` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `actualizar_inventario` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `actualizar_inventario` AFTER INSERT ON `inventario` FOR EACH ROW BEGIN
    CALL CalcularInventario();
END */$$


DELIMITER ;

/* Trigger structure for table `ventas` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `calcular_iva` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `calcular_iva` AFTER INSERT ON `ventas` FOR EACH ROW BEGIN
  DECLARE iva FLOAT;
  SET iva = NEW.total * 0.16;
  INSERT INTO impuestos (fk_venta, iva_total) VALUES (NEW.folio, iva);
END */$$


DELIMITER ;

/* Procedure structure for procedure `ActualizarContrasenaEmpleado` */

/*!50003 DROP PROCEDURE IF EXISTS  `ActualizarContrasenaEmpleado` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `ActualizarContrasenaEmpleado`(
    IN p_id_empleado INT,
    IN p_nueva_contrasena VARCHAR(255)
)
BEGIN
    DECLARE nueva_contrasena_hash VARCHAR(255);
    
    SET nueva_contrasena_hash = SHA2(p_nueva_contrasena, 256);
    
    UPDATE empleado 
    SET contrasena_hash = nueva_contrasena_hash
    WHERE id_empleado = p_id_empleado;
END */$$
DELIMITER ;

/* Procedure structure for procedure `CalcularHashContrasena` */

/*!50003 DROP PROCEDURE IF EXISTS  `CalcularHashContrasena` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CalcularHashContrasena`(IN p_contrasena VARCHAR(255), OUT p_hash VARCHAR(255))
BEGIN
  SET p_hash = SHA2(p_contrasena, 256);
END */$$
DELIMITER ;

/* Procedure structure for procedure `CalcularInventario` */

/*!50003 DROP PROCEDURE IF EXISTS  `CalcularInventario` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CalcularInventario`()
BEGIN
    -- Primero selecciona los ingredientes como antes
    SELECT ingrediente.id_ingrediente AS id, ingrediente.nombre, SUM(inventario.cantidad) AS total_disponible
    FROM ingrediente
    INNER JOIN inventario ON ingrediente.id_ingrediente = inventario.fk_ingrediente
    GROUP BY ingrediente.id_ingrediente, ingrediente.nombre
    UNION ALL  -- luego une con la selección de productos que no son platillos
    SELECT producto.id_producto AS id, producto.descripcion AS nombre, producto.cantidad_en_stock AS total_disponible
    FROM producto
    WHERE producto.es_platillo = 0;
END */$$
DELIMITER ;

/* Procedure structure for procedure `DineroEfectivo` */

/*!50003 DROP PROCEDURE IF EXISTS  `DineroEfectivo` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `DineroEfectivo`(IN fecha_inicio DATE, IN fecha_fin DATE, OUT dinero_efectivo FLOAT)
BEGIN
    SELECT SUM(total) 
    INTO dinero_efectivo
    FROM ventas 
    WHERE fecha BETWEEN fecha_inicio AND fecha_fin
    AND fk_formasDePago IN (
        SELECT id_formaDePago
        FROM formadepago
        WHERE efectivo = 1
    );
END */$$
DELIMITER ;

/* Procedure structure for procedure `InsertarEmpleado` */

/*!50003 DROP PROCEDURE IF EXISTS  `InsertarEmpleado` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertarEmpleado`(
    IN p_nombre VARCHAR(50),
    IN p_telefono VARCHAR(20),
    IN p_cargo VARCHAR(30),
    IN p_contrasena VARCHAR(255)
)
BEGIN
    DECLARE contrasena_hash VARCHAR(255);
    
    SET contrasena_hash = SHA2(p_contrasena, 256);
    
    INSERT INTO empleado (nombre, telefono, cargo, contrasena_hash)
    VALUES (p_nombre, p_telefono, p_cargo, contrasena_hash);
END */$$
DELIMITER ;

/* Procedure structure for procedure `MostrarDetallesVentaLegibles` */

/*!50003 DROP PROCEDURE IF EXISTS  `MostrarDetallesVentaLegibles` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `MostrarDetallesVentaLegibles`()
BEGIN
    SELECT ventas.folio, ventas.fecha, cliente.nombre AS nombre_cliente, producto.descripcion AS nombre_producto, detallesorden.cantidad, detallesorden.precio_unidad, (detallesorden.cantidad * detallesorden.precio_unidad) AS total
    FROM detallesorden
    INNER JOIN ventas ON detallesorden.fk_venta = ventas.folio
    INNER JOIN cliente ON ventas.fk_cliente = cliente.id_cliente
    INNER JOIN producto ON detallesorden.fk_producto = producto.id_producto;
END */$$
DELIMITER ;

/* Procedure structure for procedure `MostrarDetallesVentaLegiblesFechas` */

/*!50003 DROP PROCEDURE IF EXISTS  `MostrarDetallesVentaLegiblesFechas` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `MostrarDetallesVentaLegiblesFechas`(IN fecha_venta DATE)
BEGIN
    SELECT ventas.folio, ventas.fecha, cliente.nombre AS nombre_cliente, producto.descripcion AS nombre_producto, detallesorden.cantidad, detallesorden.precio_unidad, (detallesorden.cantidad * detallesorden.precio_unidad) AS total
    FROM detallesorden
    INNER JOIN ventas ON detallesorden.fk_venta = ventas.folio
    INNER JOIN cliente ON ventas.fk_cliente = cliente.id_cliente
    INNER JOIN producto ON detallesorden.fk_producto = producto.id_producto
    WHERE ventas.fecha = fecha_venta;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
